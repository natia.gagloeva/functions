function nThNoRepeatedValue(arr, n) {
    const counts = {};
    const nonRepeatedValues = [];

    
    for (const num of arr) {
        counts[num] = (counts[num] || 0) + 1;
    }

   
    for (const num of arr) {
        if (counts[num] === 1) {
            nonRepeatedValues.push(num);
        }
    }

   
    return nonRepeatedValues[n - 1];
}

console.log(nThNoRepeatedValue([321, 43, 3213, 1689], 2)); 



function nThNoRepeatedValue(arr, n) {
    const counts = {};
    const nonRepeatedValues = [];

    for (const num of arr) {
        counts[num] = (counts[num] || 0) + 1;
    }


    for (const num of arr) {
        if (counts[num] === 1) {
            nonRepeatedValues.push(num);
        }
    }

    return nonRepeatedValues[n - 1];
}

console.log(nThNoRepeatedValue([1, 1, 3, 4, 3, 10], 1)); 


function nThNoRepeatedValue(arr, n) {
    const counts = {};
    const nonRepeatedValues = [];

    for (const num of arr) {
        counts[num] = (counts[num] || 0) + 1;
    }

    
    for (const num of arr) {
        if (counts[num] === 1) {
            nonRepeatedValues.push(num);
        }
    }

    return nonRepeatedValues[n - 1];
}


console.log(nThNoRepeatedValue([1, 2, 1, 1], 1)); 


function isPrime(num) {
    if (num <= 1) {
        return false;
    }

    for (let i = 2; i <= Math.sqrt(num); i++) {
        if (num % i === 0) {
            return false;
        }
    }

    return true;
}

function primeValues(arr) {
    return arr.map(num => isPrime(num));
}

console.log(primeValues([4, 2, 7, 10, 13])); 



function isPrime(num) {
    if (num <= 1) {
        return false;
    }

    for (let i = 2; i <= Math.sqrt(num); i++) {
        if (num % i === 0) {
            return false;
        }
    }

    return true;
}

function primeValues(arr) {
    return arr.map(num => isPrime(num));
}


console.log(primeValues([17, 3, 21])); 